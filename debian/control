Source: timbl
Section: science
Priority: optional
Maintainer: Debian Science Team <debian-science-maintainers@lists.alioth.debian.org>
Uploaders:
 Joost van Baal-Ilić <joostvb@debian.org>,
 Ko van der Sloot <ko.vandersloot@uvt.nl>,
 Maarten van Gompel <proycon@anaproy.nl>,
Build-Depends:
 dpkg-dev (>= 1.22.5),
 debhelper-compat (= 13),
 libticcutils-dev (>=0.36),
 autoconf-archive,
 libxml2-dev,
 pkgconf,
Standards-Version: 4.7.0
Rules-Requires-Root: no
Homepage: https://languagemachines.github.io/timbl/
Vcs-Git: https://salsa.debian.org/science-team/timbl.git
Vcs-Browser: https://salsa.debian.org/science-team/timbl

Package: timbl
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Description: Tilburg Memory Based Learner
 Memory-Based Learning (MBL) is a machine-learning method applicable to a wide
 range of tasks in Natural Language Processing (NLP).
 .
 The Tilburg Memory Based Learner, TiMBL, is a tool for NLP research, and for
 many other domains where classification tasks are learned from examples.  It
 is an efficient implementation of k-nearest neighbor classifier.
 .
 TiMBL's features are:
  * Fast, decision-tree-based implementation of k-nearest neighbor
 classification;
  * Implementations of IB1 and IB2, IGTree, TRIBL, and TRIBL2 algorithms;
  * Similarity metrics: Overlap, MVDM, Jeffrey Divergence, Dot product, Cosine;
  * Feature weighting metrics: information gain, gain ratio, chi squared,
 shared variance;
  * Distance weighting metrics: inverse, inverse linear, exponential decay;
  * Extensive verbosity options to inspect nearest neighbor sets;
  * Server functionality and extensive API;
  * Fast leave-one-out testing and internal cross-validation;
  * and Handles user-defined example weighting.
 .
 TiMBL is a product of the Centre of Language and Speech Technology
 (Radboud University, Nijmegen, The Netherlands), the ILK Research Group
 (Tilburg University, The Netherlands) and the CLiPS Research Centre
 (University of Antwerp, Belgium).
 .
 If you do scientific research in NLP, timbl will likely be of use to you.

Package: libtimbl-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends:
 libtimbl7 (= ${binary:Version}),
 ${misc:Depends},
Conflicts:
 libtimbl3-dev,
 libtimbl4-dev,
Replaces:
 libtimbl3-dev,
 libtimbl4-dev,
Description: Tilburg Memory Based Learner - development
 The Tilburg Memory Based Learner, TiMBL, is a tool for Natural Language
 Processing research, and for many other domains where classification tasks are
 learned from examples.  It is an efficient implementation of k-nearest neighbor
 classifier.
 .
 TiMBL is a product of the Centre of Language and Speech Technology
 (Radboud University, Nijmegen, The Netherlands), the ILK Research Group
 (Tilburg University, The Netherlands) and the CLiPS Research Centre
 (University of Antwerp, Belgium).
 .
 This package provides the TiMBL header files required to compile C++ programs
 that use TiMBL.

Package: libtimbl7
Section: libs
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Description: Tilburg Memory Based Learner - runtime
 The Tilburg Memory Based Learner, TiMBL, is a tool for Natural Language
 Processing research, and for many other domains where classification tasks are
 learned from examples.  It is an efficient implementation of k-nearest neighbor
 classifier.
 .
 TiMBL is a product of the Centre of Language and Speech Technology
 (Radboud University, Nijmegen, The Netherlands), the ILK Research Group
 (Tilburg University, The Netherlands) and the CLiPS Research Centre
 (University of Antwerp, Belgium).
 .
 This package provides the runtime files required to run programs that use
 TiMBL.
